# Semestrální práce
## Přebarvování souvislých oblastí ve snímku

**Použití aplikace**
- **GNU/Linux** a nástroj **make**
	- 1) make
	- 2) ./run_ccl vstupni_obrazek.pgm vystup.pgm

- **Microsoft Windows** a nástroj **MinGW**
	- 1) mingw32-make -f makefile.win 
	- 2) run_ccl.exe vstupni_obrazek.pgm vystup.pgm
