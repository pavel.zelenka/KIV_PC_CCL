#ifndef PGM2_IMAGE_H
#define PGM2_IMAGE_H

/**
 * Struktura obrazku
 */
typedef struct Image {
    int col;
    int row;
    int max;
    int ** array;
} Image;

/**
 * Kontrola typu souboru
 * @param input_file ukazatel na soubor
 * @param buffer ukazatel na buffer
 * @return vrati 1 pri uspesnem nacteni
 */
int checkFileFormat(FILE * input_file, char * buffer);

/**
 * Vypsani komentaru
 * @param input_file ukazatel na soubor
 * @param buffer ukazatel na buffer
 */
void readComments(FILE * input_file, char * buffer);

/**
 * Nacteni parametru obrazku
 * @param input_file ukazatel na soubor
 * @param buffer ukazatel na buffer
 * @param col ukazatel na sloupce
 * @param row ukazatel na radky
 * @param max ukazatel na max. hodnotu sedi
 */
void readParams(FILE * input_file, char * buffer, int * col, int * row, int * max);

/**
 * Nacteni obrazku
 * @param input_file soubor
 * @param buffer
 * @param image obrazek
 * @return vrati 1 pri uspesnem nacteni
 */
int readImage(FILE * input_file, char * buffer, Image * image);

/**
 * Vypise obrazek na obrazovku
 * (vhodne pro testovani s obrazky malych rozmeru)
 * @param image obrazek
 */
void printImage(Image * image);

/**
 * Zapsani do souboru
 * @param output_file ukazatel na soubor pro zapis
 * @param image ukazatel na obrazek
 */
void writeImage(FILE * output_file, Image * image);

/**
 * Zruseni obrazku a uvolneni pameti
 * @param image ukazatel na obrazek
 */
void freeImage(Image * image);

#endif //PGM2_IMAGE_H
