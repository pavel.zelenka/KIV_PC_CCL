#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "image.h"
#include "ccl.h"

#define WRONG_ARGUMENTS_MSG "Connected Component Labeling\nUsage: ccl.exe [input] [output]\n"
#define CANNOT_BE_OPENED_MSG "File \"%s\" cannot be opened!\n"
#define INPUT_LOC_MSG "Input File Location = \"%s\"\n"
#define OUTPUT_LOC_MSG "Output File Location = \"%s\"\n"
#define COMPLETE_MSG "Complete!\n"
#define FILENAME_EXTENSION ".pgm"
#define BUFFER_SIZE 255

void extensionCheck(char * filename);

/**
 * Spusteni programu
 * @param argc pocet argumentu
 * @param argv pole argumentu
 */
int main(int argc, char *argv[]) {

    char * input_file_loc;
    char * output_file_loc;

    // Kontrola poctu argumentu
    if(argc != 3) {
        printf(WRONG_ARGUMENTS_MSG);
        exit(0);
    } else {
        input_file_loc = malloc((strlen(argv[1])+5)*sizeof(char));
        strcpy(input_file_loc,argv[1]);
        extensionCheck(input_file_loc);
        printf(INPUT_LOC_MSG,input_file_loc);
        output_file_loc = malloc((strlen(argv[2])+5)*sizeof(char));
        strcpy(output_file_loc,argv[2]);
        extensionCheck(output_file_loc);
        printf(OUTPUT_LOC_MSG,output_file_loc);

        /* Nacteni vstupniho a vystupniho souboru z klavesnice pro testovani
        printf("Insert Input File Location: ");
        scanf("%s", input_file_loc);
        printf("Insert Output File Location: ");
        scanf("%s", output_file_loc);
        */
    }

    // Otevreni souboru
    FILE * input_file;
    input_file = fopen(input_file_loc,"r");     // otevrit soubor pro cteni
    if(input_file == NULL) {                    // soubor nelze otevrit
        printf(CANNOT_BE_OPENED_MSG, input_file_loc);
        exit(0);
    }

    // Nacteni obrazku
    int col, row, max;
    char buffer[BUFFER_SIZE];
    checkFileFormat(input_file, buffer);
    readComments(input_file, buffer);
    readParams(input_file, buffer, &col, &row, &max);

    // Vytvoreni struktury obrazku
    Image * input_image = (Image *) malloc (sizeof(Image));
    input_image->col = col;
    input_image->row = row;
    input_image->max = max;

    readImage(input_file, buffer, input_image);

    fclose(input_file);

    // Vypsani hodnot obrazku na konzoli (pri ladeni aplikace)
    //printImage(input_image);
    //printf("columns=%d; rows=%d; max=%d\n", image->col, image->row, image->max);

    // Ulozeni obrazku
    Image * output_image = NULL;
    output_image = runCCLAlgorithm(input_image);
    //printImage(output_image);     // vypsani hodnot obrazku na konzoli (pri ladeni aplikace)
    FILE * output_file;
    if(output_image) {
        output_file = fopen(output_file_loc,"wb");
        writeImage(output_file, output_image);
        fclose(output_file);
    }

    // Uvolneni pameti
    freeImage(input_image);
    input_image = NULL;
    freeImage(output_image);
    output_image = NULL;
    free(input_file_loc);
    input_file_loc = NULL;
    free(output_file_loc);
    output_file_loc = NULL;

    // Hotovo
    printf(COMPLETE_MSG);
    return 0;
}

/**
 * Kontrola a validace nazvu souboru
 * @param filename ukazatel na nazev souboru
 */
void extensionCheck(char * filename) {
    char * last_dot;                        // ukazatel na posledni tecku v souboru
    last_dot = strrchr(filename, '.');
    if(last_dot != NULL ) {
        if(strcmp(last_dot, FILENAME_EXTENSION) != 0) {     // neni-li navratova hodnota 0, soubor nekonci na pozadovanou priponu
            strcat(filename, FILENAME_EXTENSION);
        //    printf("EXTENSION ADDED\n");
        } else {
        //    printf("EXTENSION OK\n");
        }
    } else { // nazev souboru neobsahuje priponu
        strcat(filename, FILENAME_EXTENSION);
    //    printf("EXTENSION ADDED\n");
    }
}