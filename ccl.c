#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "image.h"
#include "ccl.h"

#define BG_COLOR 0
#define COLLISIONS_TABLE_SIZE 2
#define MAX_GRAY_VALUE 255

/**
 * Vyhledani propojenych komponent
 * @param input_image ukazatel na vstupni obrazek
 * @return ukazatel na vystupni obrazek
 */
Image * runCCLAlgorithm(Image * input_image) {
    int i;

    // tabulka ekvivalence barev
    Collisions * collisions = (Collisions *) malloc (sizeof(Collisions));
    collisions->size = COLLISIONS_TABLE_SIZE;                               // velikost tabulky kolizi
    collisions->used = 0;                                                   // pocet pouzitych oznaceni
    collisions->array = malloc(sizeof(int*)*collisions->size);              // tabulka kolizi

    // vychozi hodnoty tabulky kolizi (hodnota = index)
    for (i=0; i<collisions->size; i++) {
        collisions->array[i] = i;
    }


    // obarveny obrazek
    Image * output_image = (Image *) malloc (sizeof(Image));
    output_image->col = input_image->col;
    output_image->row = input_image->row;
    output_image->max = input_image->max;
    output_image->array = malloc(sizeof(int*)*output_image->row);
    for (i=0; i<output_image->row; i++ ) {
        output_image->array[i] = malloc(sizeof(int) * output_image->col);
    }

    // pocet oznacenych komponent, muze dochazet ke kolizim
    int shaky_label_count;

    // prvni pruchod algoritmu CCL
    shaky_label_count = firstPass(input_image, output_image, collisions);

    // osetreni poctu komponent porovnanim s tabulkou kolizi
    int final_label_count;
    final_label_count = countUniqueLabels(collisions, shaky_label_count);

    // druhu pruchod algortmu CCL
    secondPass(output_image, collisions, final_label_count);

    // vypise tabulku kolizi
    //printCollisionTable(collisions);

    // uvolneni pameti
    freeCollisionTable(collisions);
    collisions = NULL;

    // vrati ukazatel na obarveny obrazek
    return output_image;
}

/**
 * Vrati osetreny pocet komponent po porovnani s tabulkou kolizi
 * @param collisions tabulka kolizi
 * @param shaky_label_count neosetreny pocet komponent
 * @return pocet komponent po osetreni s tabulkou kolizi
 */
int countUniqueLabels(Collisions * collisions, int shaky_label_count) {
    int i, j;
    int final_label_count;
    final_label_count = 0;
    for(i = 0; i <= shaky_label_count; i++) {
        if(collisions->array[i] == i) {
            for(j = 0; j <= shaky_label_count; j++) {
                if(collisions->array[j] == i) {
                    collisions->array[j] = final_label_count;
                }
            }
            final_label_count++;
        }
    }
    return final_label_count;
}

/**
 * Prvni pruchod algoritmu CCL
 * @param input_image vstupni onbrazek
 * @param output_image vystupni obrazek
 * @param collisions tabulka kolizi
 * @return pocet oznacenych komponent, ktery neni osetren s tabulkou kolizi
 */
int firstPass(Image * input_image, Image * output_image, Collisions * collisions) {
    int i, j;
    int shaky_label_count;    // pocet komponent (neosetreny s tabulkou kolizi)
    int value;                // hodnota na pozici
    int neighbor_value;       // hodnota souseda
    int assigned;             // prirazena hodnota
    shaky_label_count = 0;
    value = 0;
    neighbor_value = 0;
    assigned = 0;

    // prvni pruchod
    for(i = 0; i < output_image->row; i++) {
        for (j = 0; j < output_image->col; j++) {
            value = input_image->array[i][j];
            if(value == BG_COLOR) {
                output_image->array[i][j] = BG_COLOR;
            } else if(value != BG_COLOR) {
                // zatim neprirazeno
                assigned = 0;
                neighbor_value = 0;

                // existuje levy horni soused
                if(i != 0 && j != 0) {
                    neighbor_value = output_image->array[i-1][j-1];
                    if(neighbor_value != BG_COLOR) {
                        if(assigned == 0) {
                            output_image->array[i][j] = neighbor_value;
                            assigned = neighbor_value;
                        } else {        // Kontrola kolize
                            collisionCheck(collisions, assigned, neighbor_value);
                        }
                    }
                }

                // existuje horni soused
                if(i != 0) {
                    neighbor_value = output_image->array[i-1][j];
                    if(neighbor_value != BG_COLOR) {
                        if(assigned == 0) {
                            output_image->array[i][j] = neighbor_value;
                            assigned = neighbor_value;
                        } else {        // Kontrola kolize
                            collisionCheck(collisions, assigned, neighbor_value);
                        }
                    }
                }

                // existuje pravy horni soused
                if(i != 0 && j < (output_image->col-1)) {
                    neighbor_value = output_image->array[i-1][j+1];
                    if(neighbor_value != BG_COLOR) {
                        if(assigned == 0) {
                            output_image->array[i][j] = neighbor_value;
                            assigned = neighbor_value;
                        } else {        // Kontrola kolize
                            collisionCheck(collisions, assigned, neighbor_value);
                        }
                    }
                }

                // existuje levy soused
                if(j != 0) {
                    neighbor_value = output_image->array[i][j-1];
                    if(neighbor_value != BG_COLOR) {
                        if(assigned == 0) {
                            output_image->array[i][j] = neighbor_value;
                            assigned = neighbor_value;
                        } else {        // Kontrola kolize
                            collisionCheck(collisions, assigned, neighbor_value);
                        }
                    }
                }

                // stale neprirazeno
                if(assigned == 0) {
                    shaky_label_count++;                // neosetreny pocet komponent
                    collisions->used++;                 // pocet pouzitych oznaceni
                    if(collisions->used == collisions->size) {
                        // zvetsit tabulku kolizi
                        doubleCollisionTable(collisions);
                    }

                    output_image->array[i][j] = (shaky_label_count);
                }
            }
        }
    }
    return shaky_label_count;
}

/**
 * Druhy pruchod algoritmu CCL
 * @param output_image vystupni obrazek
 * @param collisions tabulka kolizi
 * @param final_label_count finalni pocet komponent
 */
void secondPass(Image * output_image, Collisions * collisions, int final_label_count) {
    int i,j;
    int value;          // hodnota na prochazene pozici
    int new_value;      // nova hodnota na prochazene pozici

    // druhy pruchod
    for(i = 0; i < output_image->row; i++) {
        for (j = 0; j < output_image->col; j++) {
            value = output_image->array[i][j];
            if(value != BG_COLOR) {
                //output_image->array[i][j] = collisions->array[value];     // hodnoty odpovidaji cislu komponenty (pro ladeni aplikace)
                new_value = collisions->array[value];
                // upraveni stupne sedi
                output_image->array[i][j] = MAX_GRAY_VALUE - ((new_value * (MAX_GRAY_VALUE / final_label_count)) % MAX_GRAY_VALUE);
            } else {
                output_image->array[i][j] = 0;
            }
        }
    }
}

/**
 * Zdvojnasobi velikost tabulky kolizi
 * @param collisions tabulka kolizi
 */
void doubleCollisionTable(Collisions * collisions) {
    int k;
    k = collisions->size;                           // puvodni velikost
    collisions->size = collisions->size*2;          // nova velikost
    collisions->array = realloc(collisions->array, sizeof(int*)*collisions->size);

    // doplneni tabulky kolizi (hodnota = index)
    for(k; k<collisions->size; k++) {
        collisions->array[k] = k;
    }
}

/**
 * Vypise tabulku kolizi
 * @param collisions tabulka kolizi
 */
void printCollisionTable(Collisions * collisions) {
    int i;
    for(i = 0; i < collisions->size; i++) {
        printf(" %d ", collisions->array[i]);
    }
    printf("\n");
}

/**
 * Uvolneni pameti po tabulce kolizi
 * @param collisions tabulka kolizi
 */
void freeCollisionTable(Collisions * collisions) {
    if(collisions) {
        free(collisions->array);
        free(collisions);
    }
}

/**
 * Kontrola kolizi
 * @param collisions tabulka kolizi
 * @param val1 hodnota 1
 * @param val2 hodnota 2
 */
void collisionCheck(Collisions * collisions, int val1, int val2) {
    int i;
    int old_value;          //pomocna promenna pro ukladani hodnot na prochazene pozici
    if(val1 != val2) {
        if(val1 > val2) {
            // nahradi hodnotu na pozici val1 hodnotou na pozici val2 v tabulce kolizi
            if(collisions->array[val1] > collisions->array[val2])  {
                old_value = collisions->array[val1];
                collisions->array[val1] = collisions->array[val2];
                // vsechny pozice v tabulce kolizi odkazujici na hodnotu na pozici val1 budou nyni odkazovat na hodnotu na pozici val2
                for(i = 0; i < collisions->size; i++) {
                    if(collisions->array[i] == old_value) {
                        collisions->array[i] = collisions->array[val2];
                    }
                }
            }
            // nahradi hodnotu na pozici val2 hodnotou na pozici val1 v tabulce kolizi
            if(collisions->array[val2] > collisions->array[val1])  {
                old_value = collisions->array[val1];
                collisions->array[val2] = collisions->array[val1];
                // vsechny pozice v tabulce kolizi odkazujici na hodnotu na pozici val2 budou nyni odkazovat na hodnotu na pozici val1
                for(i = 0; i < collisions->size; i++) {
                    if(collisions->array[i] == old_value) {
                        collisions->array[i] = collisions->array[val1];
                    }
                }
            }
        } else {
            // nahradi hodnotu na pozici val2 hodnotou na pozici val1 v tabulce kolizi
            if(collisions->array[val2] > collisions->array[val1])  {
                old_value = collisions->array[val2];
                collisions->array[val2] = collisions->array[val1];
                // vsechny pozice v tabulce kolizi odkazujici na hodnotu na pozici val2 budou nyni odkazovat na hodnotu na pozici val1
                for(i = 0; i < collisions->size; i++) {
                    if(collisions->array[i] == old_value) {
                        collisions->array[i] = collisions->array[val1];
                    }
                }
            }
            // nahradi hodnotu na pozici val1 hodnotou na pozici val2 v tabulce kolizi
            if(collisions->array[val1] > collisions->array[val2])  {
                old_value = collisions->array[val1];
                collisions->array[val1] = collisions->array[val2];
                // vsechny pozice v tabulce kolizi odkazujici na hodnotu na pozici val1 budou nyni odkazovat na hodnotu na pozici val2
                for(i = 0; i < collisions->size; i++) {
                    if(collisions->array[i] == old_value) {
                        collisions->array[i] = collisions->array[val2];
                    }
                }
            }
        }
    }
}