#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "image.h"

#define COMMENT_SYMBOL '#'
#define COMMENT_MSG "Comment: %s \n"
#define WRONG_IMAGE_ERR "The image is NOT valid a PGM binary file!\n"
#define IMAGE_SIZE_ERR "The image has a wrong size!\n"
#define GRAY_VALUE_ERR "The image is NOT in gray tones!\n"
#define MIN_GRAY_VALUE 0
#define MAX_GRAY_VALUE 255
#define NEW_LINE_TXT "\r\n"
#define P5_TXT "P5"
#define P5_OK "PGM binary file.\n"

/**
 * Kontrola typu souboru
 * @param input_file ukazatel na soubor
 * @param buffer ukazatel na buffer
 * @return vrati 1 pri uspesnem nacteni
 */
int checkFileFormat(FILE * input_file, char * buffer) {
    char str_p5[12] = P5_TXT;

    fscanf(input_file,"%s",buffer);
    if(strcmp(buffer, str_p5) == 0) {
        printf(P5_OK);
        return 1;
    } else {
        printf(WRONG_IMAGE_ERR);
        exit(0);
    }
}

/**
 * Vypsani komentaru
 * @param input_file ukazatel na soubor
 * @param buffer ukazatel na buffer
 */
void readComments(FILE * input_file, char * buffer) {
    fscanf(input_file,"%s", buffer);
    while (buffer[0] == COMMENT_SYMBOL){
        fgets(buffer,255,input_file);
        printf(COMMENT_MSG, buffer);
        fscanf(input_file,"%s",buffer);
    }
}

/**
 * Nacteni parametru obrazku
 * @param input_file ukazatel na soubor
 * @param buffer ukazatel na buffer
 * @param col ukazatel na sloupce
 * @param row ukazatel na radky
 * @param max ukazatel na max. hodnotu sedi
 */
void readParams(FILE * input_file, char * buffer, int * col, int * row, int * max) {
    // Nacteni hlavicky
    *col = atoi(buffer);                // nastaveni sloupcu
    fscanf(input_file,"%d",row);        // nastaveni radku
    fscanf(input_file,"%d",max);        // nastaveni max. hodnoty sedi

    // Kontrola platnosti poctu sloupcu a radek
    if(*col <= 0 || *row <= 0) {
        printf(IMAGE_SIZE_ERR);
        printf("col=%d; row=%d; max=%d\n", *col, *row, *max);
        exit(0);
    }

    // Kontrola platnosti maximalni hodnoty sedi
    if(*max < MIN_GRAY_VALUE || *max > MAX_GRAY_VALUE) {
        printf(GRAY_VALUE_ERR);
        exit(0);
    }
}

/**
 * Nacteni obrazku
 * @param input_file soubor
 * @param buffer
 * @param image obrazek
 * @return vrati 1 pri uspesnem nacteni
 */
int readImage(FILE * input_file, char * buffer, Image * image) {
    unsigned char buff[1];
    fread(buffer,sizeof(buff),1,input_file);    // Posun o 1 Byte
    int i, j;
    image->array = malloc(sizeof(int*)*image->row);
    for ( i=0; i<image->row; i++ ) {
        image->array[i] = malloc(sizeof(int) * image->col);
    }
    for(i = 0; i < image->row; i++) {
        for (j = 0; j < image->col; j++) {
            fread(buff,sizeof(buff),1,input_file);
            if(*buff >= MIN_GRAY_VALUE && *buff <= MAX_GRAY_VALUE) {
                image->array[i][j] = *buff;
            } else {
                printf(GRAY_VALUE_ERR);
                exit(0);
            }
        }
    }
    return 1;
}

/**
 * Vypise obrazek na obrazovku
 * (vhodne pro testovani s obrazky malych rozmeru)
 * @param image obrazek
 */
void printImage(Image * image) {
    int i, j;
    for (int i = 0; i < image->row; i++) {
        for(int j = 0; j < image->col; j++) {
            printf("%3d ", image->array[i][j]);
        }
        printf("\n");
    }
}

/**
 * Zapsani do souboru
 * @param output_file ukazatel na soubor pro zapis
 * @param image ukazatel na obrazek
 */
void writeImage(FILE * output_file, Image * image) {
    // hlavicka souboru
    char text_header[45] = "P5\r\n";                    // Radek informujici o binarnim ulozeni obrazku
    char text_width_height[21];                         // Radek informujici o poctu sloupcu a radku
    sprintf(text_width_height, "%d", image->col);
    strcat(text_width_height, " ");
    char text_height[10];                               // Pocet radku
    sprintf(text_height, "%d", image->row);
    strcat(text_height, NEW_LINE_TXT);
    strcat(text_width_height, text_height);
    char text_max_gray[10];                             // Radek informujici o maximalni hodnote sedi
    sprintf(text_max_gray, "%d", image->max);
    strcat(text_max_gray, NEW_LINE_TXT);
    strcat(text_header, text_width_height);
    strcat(text_header, text_max_gray);
    // zapsani hlavicky souboru
    fwrite(text_header, sizeof(char), strlen(text_header), output_file);
    // zapsani hlavicky souboru bez LF (pote se zobrazi konecne jak ma, bohuzel validator to tak nechce)
    //fwrite(text_header, sizeof(char), strlen(text_header) - 1, output_file);

    // obrazek
    int i, j;
    for (int i = 0; i < image->row; i++) {
        for (int j = 0; j < image->col; j++) {
            //printf("%3d ", image->array[i][j]);
            fwrite(&image->array[i][j], 1, 1, output_file);
        }
        //printf("\n");
    }
}

/**
 * Zruseni obrazku a uvolneni pameti
 * @param image ukazatel na obrazek
 */
void freeImage(Image * image) {
    int i;
    for (i=0; i<image->row; i++) {
        free(image->array[i]);
    }
    free(image->array);
    free(image);
}