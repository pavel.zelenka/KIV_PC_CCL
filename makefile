CC=gcc

all :	compile

compile :
	${CC} main.c image.c ccl.c -o ccl.exe

clean :
	rm -f *.o ccl
