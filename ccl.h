#ifndef PGM2_CCL_H
#define PGM2_CCL_H

/**
 * Struktura tabulky kolizi
 */
typedef struct Collisions {
    int size;
    int used;
    int * array;
} Collisions;

/**
 * Vyhledani propojenych komponent
 * @param input_image ukazatel na vstupni obrazek
 * @return ukazatel na vystupni obrazek
 */
Image * runCCLAlgorithm(Image * input_image);

/**
 * Vrati osetreny pocet komponent po porovnani s tabulkou kolizi
 * @param collisions tabulka kolizi
 * @param shaky_label_count neosetreny pocet komponent
 * @return pocet komponent po osetreni s tabulkou kolizi
 */
int countUniqueLabels(Collisions * collisions, int shaky_label_count);

/**
 * Prvni pruchod algoritmu CCL
 * @param input_image vstupni onbrazek
 * @param output_image vystupni obrazek
 * @param collisions tabulka kolizi
 * @return pocet oznacenych komponent, ktery neni osetren s tabulkou kolizi
 */
int firstPass(Image * input_image, Image * output_image, Collisions * collisions);

/**
 * Druhy pruchod algoritmu CCL
 * @param output_image vystupni obrazek
 * @param collisions tabulka kolizi
 * @param final_label_count finalni pocet komponent
 */
void secondPass(Image * output_image, Collisions * collisions, int final_label_count);

/**
 * Zdvojnasobi velikost tabulky kolizi
 * @param collisions tabulka kolizi
 */
void doubleCollisionTable(Collisions * collisions);

/**
 * Vypise tabulku kolizi
 * @param collisions tabulka kolizi
 */
void printCollisionTable(Collisions * collisions);

/**
 * Uvolneni pameti po tabulce kolizi
 * @param collisions tabulka kolizi
 */
void freeCollisionTable(Collisions * collisions);

/**
 * Kontrola kolizi
 * @param collisions tabulka kolizi
 * @param val1 hodnota 1
 * @param val2 hodnota 2
 */
void collisionCheck(Collisions * collisions, int val1, int val2);

#endif //PGM2_CCL_H
